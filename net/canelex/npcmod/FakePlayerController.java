package net.canelex.npcmod;

import net.minecraft.item.ItemStack;

import net.minecraft.network.Packet;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.network.play.client.CPacketHeldItemChange;
import net.minecraft.network.play.client.CPacketUseEntity;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.GameType;

public class FakePlayerController extends PlayerControllerMP
{
    private final Minecraft mc;
    private final NetHandlerPlayClient netClientHandler;
    private int currentPlayerItem;
    
    FakePlayerController(final Minecraft mc, final NetHandlerPlayClient pc) {
        super(mc, pc);
        this.mc = mc;
        this.netClientHandler = pc;
    }
    
/*     public void attackEntity(final EntityPlayer playerIn, final Entity attackingEntity) {
        if (attackingEntity.getEntityId() == -1) {
            playerIn.attackTargetEntityWithCurrentItem(attackingEntity);
            return;
        }
        this.syncCurrentPlayItem();
        this.netClientHandler.sendPacket((Packet)new CPacketUseEntity(attackingEntity));
        playerIn.attackTargetEntityWithCurrentItem(attackingEntity);
    }
    
   public EnumActionResult interactWithEntity(final EntityPlayer playerIn, final Entity targetEntity, ItemStack heldItem, EnumHand hand) {
        if (targetEntity.getEntityId() == -1) {
            return playerIn.interactWithEntity(targetEntity);
        }
        this.syncCurrentPlayItem();
        this.netClientHandler.sendPacket(new CPacketUseEntity(targetEntity, hand));
        return this.currentGameType == GameType.SPECTATOR ? EnumActionResult.PASS : player.interact(target, heldItem, hand);
    }

    private void syncCurrentPlayItem()
    {
        int i = this.mc.thePlayer.inventory.currentItem;

        if (i != this.currentPlayerItem)
        {
            this.currentPlayerItem = i;
            this.netClientHandler.sendPacket(new CPacketHeldItemChange(this.currentPlayerItem));
        }
    }*/
}
