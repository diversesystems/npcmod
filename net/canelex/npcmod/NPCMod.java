package net.canelex.npcmod;

import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.world.GameType;
import net.minecraft.world.WorldSettings;
import java.io.IOException;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;
import com.mojang.authlib.GameProfile;
import java.util.UUID;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraft.command.ICommand;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import me.boomboompower.EntityFakePlayer;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "npcmod", name = "NPC Mod", version = "1.1")
public class NPCMod
{
    private final Minecraft mc;
    private EntityFakePlayer summonedNPC;
    
    public NPCMod() {
        this.mc = Minecraft.getMinecraft();
        this.summonedNPC = null;
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register((Object)this);
        ClientCommandHandler.instance.registerCommand((ICommand)new CommandNPC(this));
    }
    
    @SubscribeEvent
    public void onCheckTick(final TickEvent.ClientTickEvent event) {
        if (this.summonedNPC != null) {
            this.checkController();
        }
    }
    
    void setSummonedNPC(final String name, final String[] args) throws IOException {
        if (this.mc.theWorld != null) {
            (this.summonedNPC = new EntityFakePlayer((World)this.mc.theWorld, new GameProfile(UUID.randomUUID(), name))).setPosition(this.mc.thePlayer.posX, this.mc.thePlayer.posY + 0.01, this.mc.thePlayer.posZ);
            this.mc.theWorld.addEntityToWorld(-1, (Entity)this.summonedNPC);
            for (int arg = 1; arg < args.length; ++arg) {
                final String argStr = args[arg];
                if (argStr.equals("-d")) {
                    this.summonedNPC.setItemStackToSlot(EntityEquipmentSlot.HEAD, new ItemStack(Item.getItemById(310)));
                    this.summonedNPC.setItemStackToSlot(EntityEquipmentSlot.CHEST, new ItemStack(Item.getItemById(311)));
                    this.summonedNPC.setItemStackToSlot(EntityEquipmentSlot.LEGS, new ItemStack(Item.getItemById(312)));
                    this.summonedNPC.setItemStackToSlot(EntityEquipmentSlot.FEET, new ItemStack(Item.getItemById(313)));
                }
                if (argStr.equals("-s")) {
                    this.summonedNPC.setItemStackToSlot(EntityEquipmentSlot.MAINHAND, new ItemStack(Item.getItemById(276)));
                }
                if (argStr.equals("-e")) {
                    for (int i = 0; i < 6; ++i) {
                        if (this.summonedNPC.getItemStackFromSlot(EntityEquipmentSlot.values()[i]) != null) {
                            this.summonedNPC.getItemStackFromSlot(EntityEquipmentSlot.values()[i]).addEnchantment(Enchantment.getEnchantmentByLocation("protection"), 1);
                        }
                    }
                    if (this.summonedNPC.getHeldItemMainhand() != null) {
                        this.summonedNPC.getHeldItemMainhand().addEnchantment(Enchantment.getEnchantmentByLocation("sharpness"), 1);
                    }
                }
            }
        }
    }
    
    boolean hasSummonedNPC() {
        return this.mc.theWorld != null && this.summonedNPC != null;
    }
    
    void clearSummonedNPC() {
        if (this.hasSummonedNPC()) {
            this.mc.theWorld.removeEntity((Entity)this.summonedNPC);
            this.summonedNPC = null;
        }
    }
    
    private void checkController() {
        if (!(this.mc.playerController instanceof FakePlayerController)) {
            final GameType type = this.mc.playerController.isInCreativeMode() ? GameType.CREATIVE : (this.mc.playerController.gameIsSurvivalOrAdventure() ? GameType.SURVIVAL : GameType.CREATIVE.ADVENTURE);
            (this.mc.playerController = new FakePlayerController(this.mc, this.mc.getConnection())).setGameType(type);
        }
    }
}
