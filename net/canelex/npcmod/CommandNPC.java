package net.canelex.npcmod;

import net.minecraft.command.CommandException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.CommandBase;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class CommandNPC extends CommandBase
{
    private NPCMod mod;
    
    CommandNPC(final NPCMod mod) {
        this.mod = mod;
    }
    

    
    public boolean func_71519_b(final ICommandSender sender) {
        return true;
    }


    @Override
    public String getCommandName() {
        return "npc";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "/npc <name>";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) {
            if (this.mod.hasSummonedNPC()) {
                this.mod.clearSummonedNPC();
                sender.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Removed the player NPC"));
            }
            else {
                sender.addChatMessage(new TextComponentString(TextFormatting.RED + "You do not have an npc, do /npc <playername> to create one!"));
            }
        }
        else {
            try {
                this.mod.setSummonedNPC(args[0], args);
                sender.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Created player NPC for '" + args[0] + "'."));
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
