package me.boomboompower;

import net.minecraft.client.renderer.texture.ITextureObject;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import java.awt.image.BufferedImage;
import net.minecraft.client.renderer.IImageBuffer;
import net.minecraft.client.renderer.ImageBufferDownload;
import java.io.File;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.resources.DefaultPlayerSkin;
import com.mojang.authlib.GameProfile;
import java.util.UUID;
import net.minecraft.world.World;
import net.minecraft.util.ResourceLocation;
import net.minecraft.client.entity.EntityOtherPlayerMP;

public class EntityFakePlayer extends EntityOtherPlayerMP
{
    private ResourceLocation skin;
    
    public EntityFakePlayer(final World world) {
        super(world, new GameProfile(UUID.randomUUID(), System.currentTimeMillis() + ""));
    }
    
    public EntityFakePlayer(final World worldIn, final GameProfile gameProfileIn) {
        super(worldIn, gameProfileIn);
        System.out.println(DefaultPlayerSkin.getSkinType(gameProfileIn.getId()));
        this.setSkin(gameProfileIn.getName());
    }
    
    private void setSkin(final String name) {
        if (name == null || name.isEmpty()) {
            return;
        }
        final ResourceLocation location = this.getSkin(name);
        if (location == null) {
            return;
        }
        NetworkPlayerInfo playerInfo;
        try {
            playerInfo = (NetworkPlayerInfo) ReflectUtils.<AbstractClientPlayer>findMethod(AbstractClientPlayer.class, new String[] { "getPlayerInfo", "getPlayerInfo" }, (Class<?>[])new Class[0]).invoke(this);
        }
        catch (Throwable ex) {
            this.log("Could not get the players info!", new Object[0]);
            return;
        }
        try {
            ReflectUtils.<NetworkPlayerInfo, ResourceLocation>setPrivateValue(NetworkPlayerInfo.class, playerInfo, this.skin = location, "locationSkin", "field_178865_e");
        }
        catch (Exception ex2) {
            this.log("Failed to set the players skin (NetworkPlayerInfo)", new Object[0]);
        }
    }
    
    private ResourceLocation getSkin(final String name) {
        if (name != null && !name.isEmpty()) {
            final ResourceLocation location = new ResourceLocation("skins/" + name);
            final File file1 = new File(new File("./mods/npcmod".replace("/", File.separator), "skins"), this.getGameProfile().getName());
            final File file2 = new File(file1, this.getGameProfile().getName() + ".png");
            final IImageBuffer imageBuffer = (IImageBuffer)new ImageBufferDownload();
            final ThreadDownloadImageData imageData = new ThreadDownloadImageData(file2, String.format("http://minecraft.tools/download-skin/%s", name), DefaultPlayerSkin.getDefaultSkinLegacy(), (IImageBuffer)new IImageBuffer() {
                public BufferedImage parseUserSkin(final BufferedImage image) {
                    return image;
                }
                
                public void skinAvailable() {
                    imageBuffer.skinAvailable();
                }
            });
            Minecraft.getMinecraft().renderEngine.loadTexture(location, (ITextureObject)imageData);
            return location;
        }
        return null;
    }
    
    public ResourceLocation getLocationSkin() {
        return (this.skin == null) ? super.getLocationSkin() : this.skin;
    }
    
    private void log(final String message, final Object... replacement) {
        ReflectUtils.a(message, replacement);
    }
}
