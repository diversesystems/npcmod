package me.boomboompower;

import java.util.ArrayList;
import java.util.Arrays;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import java.lang.reflect.Method;
import java.lang.invoke.MethodHandles;
import net.minecraftforge.fml.relauncher.ReflectionHelper;
import java.lang.invoke.MethodHandle;
import java.util.List;

class ReflectUtils
{
    private static List<String> ab;
    
    static <T> MethodHandle findMethod(final Class<T> clazz, final String[] methodNames, final Class<?>... methodTypes) {
        final Method method = ReflectionHelper.findMethod((Class)clazz, (Object)null, methodNames, (Class[])methodTypes);
        try {
            return MethodHandles.lookup().unreflect(method);
        }
        catch (Exception e) {
            throw new ReflectionHelper.UnableToFindMethodException(methodNames, e);
        }
    }
    
    static <T, E> void setPrivateValue(final Class<? super T> classToAccess, final T instance, final E value, final String... fieldNames) {
        try {
            ReflectionHelper.setPrivateValue((Class)classToAccess, (Object)instance, (Object)value, ObfuscationReflectionHelper.remapFieldNames(classToAccess.getName(), fieldNames));
        }
        catch (Throwable e) {
            a("No methods found for arguments: " + Arrays.toString((Object[])fieldNames) + " !", new Object[0]);
        }
    }
    
    static void a(final String a, final Object... b) {
        if (ReflectUtils.ab.contains(a)) {
            return;
        }
        System.out.println(String.format(a, b));
        ReflectUtils.ab.add(a);
    }
    
    static {
        ReflectUtils.ab = new ArrayList<String>();
    }
}
